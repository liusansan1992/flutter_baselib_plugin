import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/3/1 13:38
///@author:  lixu
///@description:View 基类,配合[BaseViewModel]使用
///封装http通用加载错误页，空白页以及正常显示页UI和逻辑，UI可自定义
///针对需要使用AutomaticKeepAliveClientMixin的场景，使用[BaseStatefulView]
class BaseStatefulView<T extends BaseCommonViewModel> extends StatefulWidget {
  ///加载成功后显示的页面
  final Widget child;

  ///加载中页面
  final Widget? loadingChild;

  ///数据为空的页面
  final Widget? emptyChild;

  ///请求失败显示的页面
  final Widget? errorChild;

  BaseStatefulView({required this.child, this.loadingChild, this.emptyChild, this.errorChild});

  @override
  _BaseStatefulViewState<T> createState() => _BaseStatefulViewState<T>();
}

class _BaseStatefulViewState<T extends BaseCommonViewModel> extends State<BaseStatefulView> with AutomaticKeepAliveClientMixin {
  late BaseView _baseView;

  @override
  void initState() {
    _baseView = BaseView<T>(
      child: widget.child,
      loadingChild: widget.loadingChild,
      emptyChild: widget.emptyChild,
      errorChild: widget.errorChild,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    LogUtils.i('BaseViewStateful', 'build:${widget.child.toString()}');
    return _baseView;
  }

  @override
  bool get wantKeepAlive => true;
}
