import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/net/http_urls.dart';
import 'package:flutter_baselib_example/module/login/model/login_result_bean.dart';
import 'package:flutter_baselib_example/module/userlist/view/user_page1.dart';
import 'package:flutter_baselib_example/module/userlist/view/user_page2.dart';

///@date:  2021/3/11 14:19
///@author:  lixu
///@description:主页面ViewModel
class HomeViewModel extends BaseCommonViewModel {
  @override
  String getTag() {
    return 'HomeViewModel';
  }

  ///跳转页面前先获取token
  Future<bool> _getToken(BuildContext context) async {
    Map<String, dynamic> map = {
      'account': '15015001500',
      'pass': '123qwe',
      'appType': 'PATIENT',
      'device': 'ANDROID',
      'push': '13065ffa4e22e63efd2',
    };

    bool isSuccess = false;
    await api.request<LoginResultBean>(
      HttpUrls.loginUrl,
      params: map,
      loadingText: '获取Token...',
      onSuccess: (LoginResultBean? bean) {
        LogUtils.e(getTag(), '获取Token成功');
        isSuccess = true;
      },
      onError: (HttpErrorBean errorBean) {
        LogUtils.e(getTag(), '获取Token失败');
      },
    );
    return isSuccess;
  }

  ///进入用户列表页面1
  Future<void> toUserListPage(BuildContext context) async {
    bool isSuccess = await _getToken(context);
    if (isSuccess) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return UserPage1();
      }));
    }
  }

  ///进入用户列表页面2
  Future<void> toUserListPage2(BuildContext context) async {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return UserPage2();
    }));
  }
}
