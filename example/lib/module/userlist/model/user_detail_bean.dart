///@date:  2021/3/2 11:20
///@author:  lixu
///@description: 用户详情对象
class UserDetailBean {
  ///头像
  String? icon;

  ///用户id
  String? userId;

  ///用户名
  String? name;

  UserDetailBean.fromJsonMap(Map<String, dynamic> map)
      : userId = map["userId"]?.toString(),
        name = map["name"],
        icon = map["icon"];
}
