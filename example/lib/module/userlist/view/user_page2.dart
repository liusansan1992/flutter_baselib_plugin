import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/util/test_utils.dart';
import 'package:flutter_baselib_example/common/util/ui_utils.dart';
import 'package:flutter_baselib_example/module/userlist/view/user_list_view.dart';
import 'package:flutter_baselib_example/module/userlist/view_model/user_list_view_model.dart';
import 'package:flutter_baselib_example/module/userlist/view_model/token_view_model.dart';

///@date:  2021/3/11
///@author:  lixu
///@description:用户列表页面2
///TODO 进入页面时调用2(多个)个接口获取数据成功后，才能显示UI
///TODO  1、获取token接口[TokenViewModel]  2、用户列表接口[UserListViewModel]
class UserPage2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('用户UserPage2(MVVM)')),
      body: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) {
            return TokenViewModel();
          }),
          ChangeNotifierProvider(create: (_) {
            return UserListViewModel();
          }),
        ],
        child: Column(
          children: [
            UIUtils.getUserPage2DemoDescWidget(),
            Divider(color: Colors.grey, height: 1),
            Expanded(
              child: BaseStatefulView<TokenViewModel>(
                child: BaseView<UserListViewModel>(
                  child: UserListView(),
                  emptyChild: Center(
                    child: Text(
                      '这是自定义请求数据为空的页面：\n数据为空，点击刷新',
                      style: TextStyle(color: Colors.blue, fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
