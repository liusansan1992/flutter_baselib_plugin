///@date:  2021/5/21 14:04
///@author:  lixu
///@description: 演示eventbus分发事件，当前对象是event类
class TestEvent {
  int code;
  String? msg;

  TestEvent(this.code, this.msg);

  @override
  String toString() {
    return 'TestEvent{code: $code, msg: $msg}';
  }
}
