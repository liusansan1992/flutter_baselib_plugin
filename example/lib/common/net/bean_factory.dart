import 'package:flutter_baselib_example/module/login/model/login_result_bean.dart';
import 'package:flutter_baselib_example/module/userlist/model/user_detail_bean.dart';

///@date:  2021/2/26 14:02
///@author:  lixu
///@description: http json解析bean
///
///TODO 所有需要解析的对象都要在下面注册，否则无法解析json为object
///TODO 为什么要用下面方式注册才能解析json？可以参考文档：https://flutter.dev/docs/development/data-and-backend/json
///TODO Flutter中是否有GSON / Jackson / Moshi?
/// 简单的回答是没有.
/// 这样的库需要使用运行时反射，这在Flutter中是禁用的。运行时反射会干扰Dart的_tree shaking_。使用_tree shaking_，
/// 我们可以在发版时“去除”未使用的代码。这可以显着优化应用程序的大小。
/// 由于反射会默认使用所有代码，因此_tree shaking_会很难工作。
/// 这些工具无法知道哪些widget在运行时未被使用，
/// 因此冗余代码很难剥离。使用反射时，应用尺寸无法轻松的进行优化。
///
class BeanFactory {
  ///json解析
  ///[jsonData] json map
  static T? parseObject<T>(Map<String, dynamic>? jsonData) {
    if (jsonData == null) {
      return null;
    } else if (T.toString() == "LoginResultBean") {
      return LoginResultBean.fromJsonMap(jsonData) as T;
    } else if (T.toString() == "UserDetailBean") {
      return UserDetailBean.fromJsonMap(jsonData) as T;
    } else {
      return jsonData as T;
    }
  }
}
