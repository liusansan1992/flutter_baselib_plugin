import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

///@date:  2021/9/22 17:22
///@author:  lixu
///@description:
class OneContextUtils {
  ///[OneContext] 全局context，用于在没有context的情况下显示dialog，跳转页面...
  static OneContext get oneContext {
    return OneContext();
  }

  static BuildContext? get context {
    return OneContext().context;
  }

  ///获取[OneContext]#[builder]
  ///在应用[MaterialApp]#[builder]使用该字段
  static TransitionBuilder get oneContextBuilder {
    return OneContext().builder;
  }

  ///获取[OneContext]#[key]
  ///在应用[MaterialApp]#[navigatorKey]使用该字段
  static GlobalKey<NavigatorState> get oneContextKey {
    return OneContext().key;
  }
}
